function extractFullName(array) {
    let fullNames = array.map((currentObject) => {
        return currentObject.first + " " + currentObject.last;
    })

    return fullNames;
}


console.log(extractFullName([{ first: 'Elie', last: "Schoppik" }, { first: 'Tim', last: "Garcia" }, { first: 'Matt', last: "Lane" }, { first: 'Colt', last: "Steele" }]));