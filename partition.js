const arr = [1, 2, 3, 4, 5, 6, 7, 8];
const names = ['Elie', 'Colt', 'Tim', 'Matt'];

function isEven(array, val) {
    if (val % 2 == 0)
        array[0].push(val);
    else
        array[1].push(val);

    return array;
}

function isLongerThanThreeChracters(array,val){
    if(val.length>3)
    array[0].push(val);
    else
    array[1].push(val);

    return array;
}

function partition(array, callback) {
    let answer = array.reduce(callback, [[],[]])
    return answer;
}

console.log(partition(arr, isEven));
console.log(partition(names,isLongerThanThreeChracters));
