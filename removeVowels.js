function removeVowels(str) {
    let charactersArray = str.split("");

    let consonantsArray = charactersArray.filter((current) => {
        current = current.toLowerCase();

        if (current != "a" && current != "e" && current != "i" && current != "o" && current != "u")
            return true;
        else
            return false;
    })

    let Ans = "";
    consonantsArray.forEach((current) => {
        Ans = Ans + current
    })

    return Ans;
}


console.log(removeVowels("Elie"));
console.log(removeVowels("TIM"));
console.log(removeVowels("ZZZZZZ"));
