function vowelCount(str) {
    let Object = {};
    let arrayOfCharacters = str.split("");
    arrayOfCharacters.forEach(element => {
        element = element.toLowerCase();
        let code = element.charCodeAt(0);

        if (code >= 97 && code <= 122) {
            if (code == 97 || code == 101 || code == 105 || code == 111 || code == 117) {
                let char = String.fromCharCode(code);
                if (Object[char] == undefined)
                    Object[char] = 1;
                else
                    Object[char] += 1;
            }
        }

    });

    return Object;
}

console.log(vowelCount("Elie"));
console.log(vowelCount("Tim"));
console.log(vowelCount("Matt"));
console.log(vowelCount("hmmm"));
console.log(vowelCount("I Am awesome and so are you"));