function doubleOddNumbers(array) {
    let doubleOdd = array.filter((current) => {
        return current % 2 != 0;
    }).map((current) => {
        return current * 2;
    })

    return doubleOdd;
}

console.log(doubleOddNumbers([1, 2, 3, 4, 5]));
console.log(doubleOddNumbers([4, 4, 4, 4, 4]));