let array = [5, 1, 2, 3, 10];

function onlyEvenValues(array) {
    let evenValuesArray = array.filter((Element) => {
        return Element % 2 == 0;
    })

    return evenValuesArray;
}

console.log(onlyEvenValues(array));
console.log(onlyEvenValues([1, 2, 3]));