function vowelCount2(str) {
    let charArray = str.split("");

    let answer = charArray.reduce((object, current) => {

        current = current.toLowerCase();
        let code = current.charCodeAt(0);
        if (code >= 97 && code <= 122) {
            let char = String.fromCharCode(code);
            if (code == 97 || code == 101 || code == 105 || code == 111 || code == 117) {
                if (object[char] == undefined)
                    object[char] = 1;
                else
                    object[char] += 1;
            }
        }

        return object;
    }, {})

    return answer;
}

console.log(vowelCount2("Elie"));
console.log(vowelCount2("Tim"));
console.log(vowelCount2("Matt"));
console.log(vowelCount2("hmmm"));
console.log(vowelCount2("I Am awesome and so are you"));