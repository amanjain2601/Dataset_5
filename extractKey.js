function extractKey(array, key) {
    let returnedValuesOfKey = array.map((currentObject) => {
        return currentObject[key];
    })
    return returnedValuesOfKey;
}

console.log(extractKey([{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }], 'name'));