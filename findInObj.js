function findInObject(array, key, searchValue) {
    let filteredValues = array.filter((currentObject) => {
        return currentObject[key] != undefined && currentObject[key] == searchValue;
    })

    return filteredValues[0];
}

console.log(findInObject([{ first: 'Elie', last: "Schoppik" }, { first: 'Tim', last: "Garcia", isCatOwner: true }, { first: 'Matt', last: "Lane" }, { first: 'Colt', last: "Steele", isCatOwner: true }], 'isCatOwner', true))