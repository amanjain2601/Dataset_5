function valTimesIndex(array) {
    let ValuesTimesIndex = array.map((current, index) => {
        return current * index;
    })

    return ValuesTimesIndex;
}

console.log(valTimesIndex([1, 2, 3]));
console.log(valTimesIndex([1, -2, -3]))