function doubleValuesWithMap(array) {
    let doubledValues = array.map((current) => {
        return current * 2;

    })

    return doubleValues
}

console.log(doubleValuesWithMap([1, 2, 3]));
console.log(doubleValuesWithMap([1, -2, -3]));