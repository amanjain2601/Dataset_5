const arr = [{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }];

function addKeyAndValue2(array, key, value) {
    let answer = array.reduce((arrayOfObject, current)=>{
        current[key] = value;
        arrayOfObject.push(current);

        return arrayOfObject;
    },[])

    return answer;
}

console.log(addKeyAndValue2(arr, 'title', 'Instructor'));