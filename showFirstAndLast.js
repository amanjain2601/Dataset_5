let array=['colt','matt', 'tim', 'test'];

function showFirstAndLast(array){
   let modifiedString= array.map((current)=>{
       return current[0]+current[current.length-1];
    })

    return modifiedString;
}

console.log(showFirstAndLast(array));
console.log(showFirstAndLast(['hi', 'goodbye', 'smile']));

