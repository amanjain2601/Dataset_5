const arr = [{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }]

function extractValue(array, key) {
    let answer = array.reduce((arrayOfObjects, current) => {
        if (current[key] != undefined)
            arrayOfObjects.push(current[key]);

        return arrayOfObjects;
    },[])

    return answer;
}

console.log(extractValue(arr, 'name'));