const users = [
    { username: 'mlewis' },
    { username: 'akagen' },
    { username: 'msmith' }
];

function findUserByUserName(array, username) {
    let answer = array.find((current) => {
        if (current["username"] == username)
            return true;
    })

    return answer;
}

console.log(findUserByUserName(users, "mlewis"))
console.log(findUserByUserName(users, "taco"))
