function addKeyAndValue(array,key,value){
    array.map((currentObject)=>{
        currentObject[key]=value;
        return currentObject;
    })

    return array;

}

console.log(addKeyAndValue([{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}], 'title', 'instructor'));