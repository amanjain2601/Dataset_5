function filterByValue(array, key) {
    let filterValues = array.filter((currentObject) => {
        return currentObject[key] != undefined
    })

    return filterValues;
}


console.log(filterByValue([{ first: 'Elie', last: "Schoppik" }, { first: 'Tim', last: "Garcia", isCatOwner: true }, { first: 'Matt', last: "Lane" }, { first: 'Colt', last: "Steele", isCatOwner: true }], 'isCatOwner'));