function find(array, searchValue) {
    let returnedValues = array.filter((Element) => {
        return Element == searchValue
    })
    return returnedValues[0];
}

console.log(find([1, 2, 3, 4, 5], 3))
console.log(find([1, 2, 3, 4, 5], 10))